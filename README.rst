
Django Split Models
-----------

If you want to split your django models.py into multiple files, this tiny bit of code can do that for you.

It loads each model from ``models/*.py`` in ``models/__init__.py``, after which Django will recognize them* and you can keep your imports as ``from models import ModelName``.

\* for Django <= 1.6, you need to add ``app_label``, see below.

Installation & Configuration:
-----------

- Install using ``pip install git+https://bitbucket.org/mverleg/django_split_models.git``

How to use it
-----------

For each app, put this in ``models/__init__.py``:

            from split_models import load_models
            __all__ = load_models(__file__, locals())

Furthermore, for Django <= 1.6, you need to set ``app_label``. In each model:

            class Meta:
                app_label = 'name_of_your_app'

You shouldn't need to update any imports; ``from models import ModelName`` should still work if ``ModelName`` is in e.g. ``models/model_name.py``

Problems
-----------

* If your app is a symbolic link or shortcut to an app with a different name, the importer will look for the other name (so it has to be in PYTHONPATH, but that defeats the point of symlinking).

License
-----------

django_split_models is available under the revised BSD license, see LICENSE.txt. You can do anything as long as you include the license, don't use my name for promotion and are aware that there is no warranty.


