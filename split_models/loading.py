
"""
	Functions to load all stuff in the directory.
	BUG: setting app_label here isn't recognized by django,
		so it needs to be set in Meta for django <= 1.6
"""

from os import listdir
from django.db.models import Model
from os.path import dirname, normpath, split
from inspect import isclass, getmembers
from importlib import import_module


def load_modules(dirpath):
	"""
		Return a list of all loaded modules directly in dir_path
	"""
	""" Go through all python files in a directory. """
	modules = []
	for filename in listdir(dirpath):
		if filename.endswith('.py') and not filename.startswith('__'):
			""" Convert to python path by assuming two levels up are needed (usually in django directory structure). """
			module_pypath = '.'.join((split(split(dirpath)[0])[1], split(dirpath)[1], filename[:-3]))
			""" Get all classes from a file. Don't catch ImportError; more often than not it's an indirect one,
				which is hard to locate when caught. """
			modul = import_module(module_pypath)
			modul._py_path = module_pypath
			modules.append(modul)
	return modules


def check_files(_file):
	"""
		Just imports the modules; doesn't do anything with the content (which can be anything)
		useful if you just want to make django admin aware of certain files.
	"""
	return load_modules(dirname(normpath(_file)))


def load_classes(_file, local_vars):
	"""
		given the __file__ and locals() of __init__.py,
		get a list of classes and put them in locals
	"""
	modules = load_modules(dirname(normpath(_file)))
	classes = {}
	for modul in modules:
		for name, cls in [(name, obj) for name, obj in getmembers(modul) if isclass(obj)]:
			if cls.__module__ == modul._py_path:
				classes[name] = cls
	""" Update locals and return models (it is recommended to assign them to __all__). """
	local_vars.update(classes)
	return classes


def load_models(_file, local_vars):
	"""
		given the __file__ and locals() of __init__.py,
		get a list of models and put them in locals
	"""
	classes = load_classes(_file, local_vars)
	models = {}
	for name, cls in classes.items():
		if issubclass(cls, Model):
			models[name] = cls
	""" Adding app_label to each model turned off, didn't work """
	""" Update locals and return models (it is recommended to assign them to __all__). """
	local_vars.update(models)
	return list(models.values())


